#!/usr/bin/env bash

mkdir -p /tmp/archiver/ || exit 1

cd /tmp/archiver/ || exit 2

time=`date +%s` || exit 3

# The archive is just a file that has links to all the recordings

# make the archive
gsutil ls gs://onebuttonapp/*.wav > files_$time.archive || exit 4
gsutil ls gs://onebuttonapp/*.opus >> files_$time.archive || exit 5

# delete the old archive
gsutil rm gs://onebuttonapp/*.archive || echo "Did not find an old archive, is this the first time we're running this?"

# upload the new archive
gsutil cp files_$time.archive gs://onebuttonapp/ || exit 6