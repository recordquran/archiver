<?php

require_once __DIR__ . '/vendor/autoload.php';
require_once "config.php";

use \Google\Cloud\Storage\StorageClient;

/**
 * Upload a file.
 *
 * @param string $bucketName the name of your Google Cloud bucket.
 * @param string $objectName the name of the object.
 * @param string $source the path to the file to upload.
 *
 * @return Psr\Http\Message\StreamInterface
 */
function upload_file($projectId, $bucketName, $objectName, $source)
{
    $config = [
        'projectId' => $projectId,
    ];

    $storage = null;

    try
    {
        $storage = new StorageClient($config);
    }
    catch(Exception $e)
    {
        error_log("Could not create StorageClient: {$e->getMessage()}");
        echo("500 internal server error");
        http_response_code(500);
        die();
    }

    $file = fopen($source, 'r');
    $bucket = $storage->bucket($bucketName);

    try
    {
        $object = $bucket->upload($file, [
            'name' => $objectName
        ]);
    }
    catch(Exception $e)
    {
        error_log("Could not upload to bucket: {$e->getMessage()}");
        echo("500 internal server error");
        http_response_code(500);
        die();
    }
    
    
    $basename = basename($source);
    $to_log = "Uploaded {$basename} to gs://{$bucketName}/{$objectName}" . PHP_EOL;
    error_log($to_log);
    printf($to_log);
}

/**
 * List Cloud Storage bucket objects.
 *
 * @param string $bucketName the name of your Cloud Storage bucket.
 *
 * @return void
 */
function list_objects($projectId, $bucketName)
{
    $config = [
        'projectId' => $projectId,
    ];

    $storage = null;

    try
    {
        $storage = new StorageClient($config);
    }
    catch(Exception $e)
    {
        error_log("Could not create StorageClient: {$e->getMessage()}");
        echo("500 internal server error");
        http_response_code(500);
        die();
    }
    
    $bucket = $storage->bucket($bucketName);

    $objects = [];
    foreach ($bucket->objects() as $object)
    {
        array_push($objects, $object->name());
    }

    return $objects;
}

function str_ends_with($haystack, $needle)
{
    $length = strlen( $needle );

    if( !$length )
    {
        return true;
    }

    return substr( $haystack, -$length ) === $needle;
}

$archive_dir = "/tmp/archive/";

if(!is_dir($archive_dir))
{
    if (!mkdir($archive_dir, 0777, true))
    {
        die('Failed to create tmp directory '. $archive_dir);
    }
}

$time = time();

// The archive is just a file that has links to all the recordings

// # make the archive

$bucket_files = list_objects(project_name(), bucket_name());

$audio_files = [];
$bucket_url = "https://storage.googleapis.com/onebuttonapp/";

foreach ($bucket_files as $file)
{
    if(!str_ends_with($file, ".wav") && !str_ends_with($file, ".opus"))
    {
        continue;
    }

    array_push($audio_files, $bucket_url . $file);
}

$archive_name = $time . '.archive';
$archive_path = $archive_dir . $archive_name;
file_put_contents($archive_path, implode(PHP_EOL, $audio_files));

// prefixing the archive_name with 'archive' because
// it is uploaded to a sub directory in the bucket called
// 'archive'
upload_file(project_name(), bucket_name(), "archive/" . $archive_name, $archive_path);

?>